#ifndef __RADIO_EVENT_H__
#define __RADIO_EVENT_H__

#include "dot_util.h"
#include "mDotEvent.h"
#include "Fota.h"

#include "mbed.h"
#include "rtos.h"

extern Timer t0;
extern int lastsuccess;
extern int receivecount;
extern int flag;

class RadioEvent : public mDotEvent
{

public:

    RadioEvent() {}

    virtual ~RadioEvent() {}

    virtual void PacketRx(uint8_t port, uint8_t *payload, uint16_t size, int16_t rssi, int16_t snr, lora::DownlinkControl ctrl, uint8_t slot, uint8_t retries, uint32_t address, bool dupRx) {
// #if ACTIVE_EXAMPLE == PEER_TO_PEER_EXAMPLE
        // mDotEvent::PacketRx(port, payload, size, rssi, snr, ctrl, slot, retries, address, dupRx);
// #else
        mDotEvent::PacketRx2(port, payload, size, rssi, snr, ctrl, slot, retries, address, dupRx);
// #endif
    }

    /*!
     * MAC layer event callback prototype.
     *
     * \param [IN] flags Bit field indicating the MAC events occurred
     * \param [IN] info  Details about MAC events occurred
     */
    virtual void MacEvent(LoRaMacEventFlags* flags, LoRaMacEventInfo* info) {

        if (mts::MTSLog::getLogLevel() == mts::MTSLog::TRACE_LEVEL) {
            std::string msg = "OK";
            switch (info->Status) {
                case LORAMAC_EVENT_INFO_STATUS_ERROR:
                    msg = "ERROR";
                    break;
                case LORAMAC_EVENT_INFO_STATUS_TX_TIMEOUT:
                    msg = "TX_TIMEOUT";
                    break;
                case LORAMAC_EVENT_INFO_STATUS_RX_TIMEOUT:
                    msg = "RX_TIMEOUT";
                    break;
                case LORAMAC_EVENT_INFO_STATUS_RX_ERROR:
                    msg = "RX_ERROR";
                    break;
                case LORAMAC_EVENT_INFO_STATUS_JOIN_FAIL:
                    msg = "JOIN_FAIL";
                    break;
                case LORAMAC_EVENT_INFO_STATUS_DOWNLINK_FAIL:
                    msg = "DOWNLINK_FAIL";
                    break;
                case LORAMAC_EVENT_INFO_STATUS_ADDRESS_FAIL:
                    msg = "ADDRESS_FAIL";
                    break;
                case LORAMAC_EVENT_INFO_STATUS_MIC_FAIL:
                    msg = "MIC_FAIL";
                    break;
                default:
                    break;
            }
            logTrace("Event: %s", msg.c_str());

            logTrace("Flags Tx: %d Rx: %d RxData: %d RxSlot: %d LinkCheck: %d JoinAccept: %d",
                     flags->Bits.Tx, flags->Bits.Rx, flags->Bits.RxData, flags->Bits.RxSlot, flags->Bits.LinkCheck, flags->Bits.JoinAccept);
            logTrace("Info: Status: %d ACK: %d Retries: %d TxDR: %d RxPort: %d RxSize: %d RSSI: %d SNR: %d Energy: %d Margin: %d Gateways: %d",
                     info->Status, info->TxAckReceived, info->TxNbRetries, info->TxDatarate, info->RxPort, info->RxBufferSize,
                     info->RxRssi, info->RxSnr, info->Energy, info->DemodMargin, info->NbGateways);
        }
        
        if (flags->Bits.Rx) {
            #if ACTIVE_EXAMPLE == PEER_TO_PEER_EXAMPLE
            logDebug("Rx %d bytes", info->RxBufferSize);
            if (info->RxBufferSize > 0) {
                // print RX data as string and hexadecimal
                std::string rx((const char*)info->RxBuffer, info->RxBufferSize);
                printf("Rx data: %s [%s]\r\n", rx.c_str(), mts::Text::bin2hexString(info->RxBuffer, info->RxBufferSize).c_str());
            }
            #elif ACTIVE_EXAMPLE == PEER_TO_PEER_EXAMPLE_PAYLOADS_20T2W4
            receivecount++;
            printf("\r\n>>> Receiving:  %d  %d  %d bytes ;\t",receivecount,t0.read_ms(),info->RxBufferSize);
            if (info->RxBufferSize > 0) {
                // print RX data as string and hexadecimal
                std::string rx((const char*)info->RxBuffer, info->RxBufferSize);
                std::string fullpayload = mts::Text::bin2hexString(info->RxBuffer, info->RxBufferSize).c_str();
                std::string payloadtime = fullpayload.substr(2,8);
                std::string sign = fullpayload.substr(0,2);
                char *c;
                printf("\r\n>>> PayloadTime: %lu [0x%s] from: %lu ;\t",strtoul(payloadtime.c_str(),&c,16), payloadtime.c_str(),strtoul(sign.c_str(),&c,16));
            }
            
            lastsuccess=t0.read_ms();
            flag = 1;

            #else

            receivecount++;
            printf(">>> Receiving:  %d  %d  %d bytes\r\n",receivecount,t0.read_ms(),info->RxBufferSize);
            if (info->RxBufferSize > 0) {
                // // print RX data as string and hexadecimal
                std::string rx((const char*)info->RxBuffer, info->RxBufferSize);
                //printf("Rx data: %s Time Hex:[%s]\r\n", rx.c_str(), mts::Text::bin2hexString(info->RxBuffer, info->RxBufferSize).c_str());
                std::string rechex = mts::Text::bin2hexString(info->RxBuffer, info->RxBufferSize).c_str();
                //std::string hex=rechex.substr(2,string::npos);
                std::string hex=rechex.substr(2,8);
                char *c;
                printf("\r\n>>> PayloadTime: %lu [0x%s] ;\t",strtoul(hex.c_str(),&c,16), hex.c_str());
                if(rechex.length()>10){
                   printf("\r\n>>> Payload: %s\r\n",rechex.substr(10,string::npos));  // Only Payload Part
               }
            }
            lastsuccess=t0.read_ms();
            flag = 1;
            #endif
        }
    }
};

#endif

