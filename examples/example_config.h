#ifndef __EXAMPLE__CONFIG_H__
#define __EXAMPLE__CONFIG_H__

#define PEER_TO_PEER_EXAMPLE    1
#define PEER_TO_PEER_EXAMPLE_PAYLOADS_20T1W12   2
#define PEER_TO_PEER_EXAMPLE_PAYLOADS_20T1W13   3
#define PEER_TO_PEER_EXAMPLE_PAYLOADS_20T2W4    4

// the active example is the one that will be compiled
#if !defined(ACTIVE_EXAMPLE)
#define ACTIVE_EXAMPLE  PEER_TO_PEER_EXAMPLE_PAYLOADS_20T2W4
#endif

// the active channel plan is the one that will be compiled
// options are :
//      CP_US915
//      CP_AU915
//      CP_EU868
//      CP_KR920
//      CP_AS923
//      CP_AS923_JAPAN
//      CP_IN865
#if !defined(CHANNEL_PLAN)
#define CHANNEL_PLAN CP_AU915
#endif

#endif
