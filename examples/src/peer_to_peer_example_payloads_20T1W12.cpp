#include "dot_util.h"
#include "Fota.h"
#include "RadioEvent.h"

#include "rtos.h"

#include "SxRadio.h"
#include "Mote.h"
#include "mDot.h"

#if ACTIVE_EXAMPLE == PEER_TO_PEER_EXAMPLE_PAYLOADS_20T1W12

/////////////////////////////////////////////////////////////////////////////
// -------------------- DOT LIBRARY REQUIRED ------------------------------//
// * Because these example programs can be used for both mDot and xDot     //
//     devices, the LoRa stack is not include2d. The libmDot library should //
//     be imported if building for mDot devices. The libxDot library       //
//     should be imported if building for xDot devices.                    //
// * https://developer.mbed.org/teams/MultiTech/code/libmDot-dev-mbed5/    //
// * https://developer.mbed.org/teams/MultiTech/code/libmDot-mbed5/        //
// * https://developer.mbed.org/teams/MultiTech/code/libxDot-dev-mbed5/    //
// * https://developer.mbed.org/teams/MultiTech/code/libxDot-mbed5/        //
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
// * these options must match between the two devices in   //
//   order for communication to be successful
/////////////////////////////////////////////////////////////
static uint8_t network_address[] = { 0x01, 0x02, 0x03, 0x04 };
static uint8_t network_session_key[] = { 0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04 };
static uint8_t data_session_key[] = { 0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04, 0x01, 0x02, 0x03, 0x04 };

mDot* dot = NULL;
lora::ChannelPlan* plan = NULL;
lora::Mote* mote = NULL;
SxRadio* sxradio = NULL;

Serial pc(USBTX, USBRX);

AnalogIn lux(XBEE_AD0);

int sendcount=0;
int failedcount=0;
int lastsend=0;
int rtrssi=0;

Timer t0;
int lastsuccess=0;
int receivecount=0;
int flag=0;

Thread newthread;

void yetAnotherThread(int payloadBytes);

void rt_rssi();

int main()
{
    t0.start();
    // Custom event handler for automatically displaying RX data
    RadioEvent events;
    uint32_t tx_frequency;
    uint8_t tx_datarate;
    uint8_t tx_power;
    uint8_t frequency_band;

    pc.baud(115200);
    pc.printf("Something");

    mts::MTSLog::setLogLevel(mts::MTSLog::TRACE_LEVEL);

#if CHANNEL_PLAN == CP_AU915
    plan = new lora::ChannelPlan_AU915();
#endif

    assert(plan);

    dot = mDot::getInstance(plan);
    assert(dot);

    logInfo("mbed-os library version: %d.%d.%d", MBED_MAJOR_VERSION, MBED_MINOR_VERSION, MBED_PATCH_VERSION);

    // start from a well-known state
    logInfo("defaulting Dot configuration");
    dot->resetConfig();

    // make sure library logging is turned on
    dot->setLogLevel(mts::MTSLog::TRACE_LEVEL);

    // attach the custom events handler
    dot->setEvents(&events);

    // update configuration if necessary
    if (dot->getJoinMode() != mDot::PEER_TO_PEER) {
        logInfo("changing network join mode to PEER_TO_PEER");
        if (dot->setJoinMode(mDot::PEER_TO_PEER) != mDot::MDOT_OK) {
            logError("failed to set network join mode to PEER_TO_PEER");
        }
    }
    frequency_band = dot->getFrequencyBand();
    switch (frequency_band) {
        case lora::ChannelPlan::US915_OLD:
        case lora::ChannelPlan::US915:
        case lora::ChannelPlan::AU915_OLD:
        case lora::ChannelPlan::AU915:
            // 500kHz channels achieve highest throughput
            // DR_8 : SF12 @ 500kHz
            // DR_9 : SF11 @ 500kHz
            // DR_10 : SF10 @ 500kHz
            // DR_11 : SF9 @ 500kHz
            // DR_12 : SF8 @ 500kHz
            // DR_13 : SF7 @ 500kHz
            // DR_0 - DR_3 (125kHz channels) available but much slower
            tx_frequency = 915500000;
            tx_datarate = lora::DR_13;
            // 915 bands have no duty cycle restrictions, set tx power to max
            tx_power = 20;
            break;

        default:
            while (true) {
                logFatal("no known channel plan in use - extra configuration is needed!");
                wait(5);
            }
    }
    // in PEER_TO_PEER mode there is no join request/response transaction
    // as long as both Dots are configured correctly, they should be able to communicate
    update_peer_to_peer_config(network_address, network_session_key, data_session_key, tx_frequency, tx_datarate, tx_power);

    //Setting Node Mode
    if(dot->setClass("C") != mDot::MDOT_OK) {
        logError("Attempt Change node mode failed");
    }

    // Disable CRC checking
    if(dot->setCrc(false) != mDot::MDOT_OK) {
        logError("Failed to disable CRC");
    }

    // save changes to configuration
    logInfo("saving configuration");
    if (!dot->saveConfig()) {
        logError("failed to save configuration");
    }



    // display configuration
    display_config();

    mote = new lora::Mote(dot->getSettings(),plan);
    while (true) {
        // join network if not joined
        if (!dot->getNetworkJoinStatus()) {
            logInfo("Join Peer to peer network\r\n");
            join_network();
        }

        if((t0.read_ms()-lastsuccess)>30000) {
            dot->resetCpu();    // Reset device if not receive anything in 30s.
        }

        //uint16_t light;
        //std::vector<uint8_t> tx_data;


        if(flag==1 || (t0.read_ms()-lastsend)>50) {
            /* Each packet = 5 bytes for time + payload + 13 bytes headers
                Low Datarate (DR_0/DR_8): Maximum: 66 bytes per packet
                     48 (48bytes + 5 bytes time + 13 bytes headers)

                Others: Maximum 242 bytes per packet
                    237 (237bytes + 5 bytes time + 13 bytes headers)
            */
            yetAnotherThread(0);
        }


        if(rtrssi==0 && receivecount > 0) {
            newthread.start(rt_rssi);
            rtrssi=1;
        }

        ThisThread::sleep_for(5);    // Replace by printout realtime RSSI
    }

    return 0;
}

void rt_rssi()
{
    while (true) {
        sxradio = mote->GetRadio();
        printf("::: RtRSSI Time: %d RSSI: %d  RegRSSI: %d\r\n",t0.read_ms(),sxradio->Rssi(sxradio->GetModem()),sxradio->Read(12)-139);
        // printf("::: RtRSSI Time: %d RSSI: %d\r\n",t0.read_ms(),Radio.GetRssi(MODEM_LORA));
        ThisThread::sleep_for(1);   // Millisec
    }
}

void yetAnotherThread(int payloadBytes)
{
    uint32_t light;
    std::vector<uint8_t> tx_data;

    light = t0.read_ms();
    tx_data.push_back(light);

    //Pushing full 4 bytes for int range time.
    tx_data.push_back((light >> 24) & 0xFF);
    tx_data.push_back((light >> 16) & 0xFF);
    tx_data.push_back((light >> 8) & 0xFF);
    tx_data.push_back(light & 0xFF);

//        for(int i=0;i<payloadBytes;i++){
//            uint8_t payloadbit = i;
//            tx_data.push_back(payloadbit & 0xFF);
//        }
    //logInfo("Time since start: %d ms, [0x%04X]\r\n", light,light);

    if(send_data(tx_data)==mDot::MDOT_OK) {
        sendcount++;
        printf("<<< Sending: No %d    Time: %d   [0x%04X]\r\n",sendcount,light,light);
        printf("<<< Payload: ");
        for (int i=0; i<tx_data.size(); i++) {
            printf("%lu ",tx_data[i]);
        }
        printf("\r\n");
        flag=0;
        lastsend=t0.read_ms();
    }
}

#endif
